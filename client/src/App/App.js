import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import Home from './pages/Home';
import Header from '../components/organisms/Header';
import Footer from '../components/organisms/Footer';

class App extends Component {
  render() {
    const App = () => (
      <div>
        <Header />
        <Switch>
          <Route exact path='/' component={Home}/>
        </Switch>
        <Footer />
      </div>
    )
    return (
      <Switch>
        <App/>
      </Switch>
    );
  }
}

export default App;