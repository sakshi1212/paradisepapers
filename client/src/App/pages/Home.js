import React, { Component } from 'react';
import Tabs from '../../components/organisms/Tabs';
import EntitiesSection from '../../components/templates/EntititesSection'
import Heading from '../../components/atoms/Heading';
import styled from 'styled-components';
import { Icon } from 'semantic-ui-react'

const HeadingWraper = styled.div`
  padding: 40px 100px;
  background: #242D34;
  text-align: left;
`

const TabsWrapper = styled.div`
  padding: 20px 100px;
`

class Home extends Component {
  render() {
    const tabs = [
      {
        tabTitle: 'Entities',
        tabContent: <EntitiesSection />,
      },
      {
        tabTitle: 'Officers',
        tabContent: 'Officers coming soon'
      },
      {
        tabTitle: 'Addresses',
        tabContent: 'Addresses coming soon'
      },
      {
        tabTitle: 'Relationships',
        tabContent: 'Relationships coming soon'
      },
    ]

    return (
    <div className="App">
      <HeadingWraper>
        <Heading level={3} palette={'#F6543A'}><Icon name="arrow left" />Back</Heading>
        <Heading level={1} palette={'white'}>Paradise Papers</Heading>
      </HeadingWraper>
      <TabsWrapper>
        <Tabs 
          tabs={tabs}
          fluid
          vertical
          tabular
        />
      </TabsWrapper>
    </div>
    );
  }
}
export default Home;