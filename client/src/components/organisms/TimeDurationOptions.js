import React, { Component } from 'react'
import { Form } from 'semantic-ui-react'
import Radio from '../atoms/Radio'
import styled from 'styled-components'

const StyledForm = styled(Form)`
  &&& {
    width: 80%;
    display: flex;
  }
`

class TimeDurationOptions extends Component {
  constructor(props){
    super(props);
    this.state = {
      value: this.props.timeSelection,
    }
  }

  handleChange = (e, { value }) => this.setState({ value }, this.props.handleTimeChange(value))
  
  render() {
    return (
      <StyledForm>
        {/* <Form.Field>
          Selected value: <b>{this.state.value}</b>
        </Form.Field> */}
        <Form.Field>
          <Radio
            label='1 Year'
            name='radioGroup'
            value='1Y'
            checked={this.state.value === '1Y'}
            onChange={this.handleChange}
          />
        </Form.Field>
        <Form.Field>
          <Radio
            label='2 Years'
            name='radioGroup'
            value='2Y'
            checked={this.state.value === '2Y'}
            onChange={this.handleChange}
          />
        </Form.Field>
        <Form.Field>
          <Radio
            label='3 Years'
            name='radioGroup'
            value='3Y'
            checked={this.state.value === '3Y'}
            onChange={this.handleChange}
          />
        </Form.Field>
        <Form.Field>
          <Radio
            label='4 Year'
            name='radioGroup'
            value='4Y'
            checked={this.state.value === '4Y'}
            onChange={this.handleChange}
          />
        </Form.Field>
        <Form.Field>
          <Radio
            label='5 Year'
            name='radioGroup'
            value='5Y'
            checked={this.state.value === '5Y'}
            onChange={this.handleChange}
          />
        </Form.Field>
      </StyledForm>
    )
  }
}

export default TimeDurationOptions;