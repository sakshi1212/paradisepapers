import React from 'react'
import styled from 'styled-components'
import { Icon } from 'semantic-ui-react'

const FooterWrapper = styled.div`
  height: 15vh;
  background: #242D34;
  padding: 10px 100px;
  display: flex;
`

const CopyRight = styled.div`
  color: white;
  width: 80%;
  display: flex;
  align-items: center
`

const IconWrapper = styled.div`
  display: flex;
  width: 30%;
  justify-content: flex-end;
  align-items: center;
`

const Footer = () => {
  const currentYear = new Date().getFullYear()
  return (
    <React.Fragment>
      <FooterWrapper>
        <CopyRight>
          © {currentYear} All Rights Reserved
        </CopyRight>
        <IconWrapper>
          <Icon circular inverted color='grey' name='facebook' />
          <Icon circular inverted color='grey' name='instagram' />
          <Icon circular inverted color='grey' name='mail' />
          <Icon circular inverted color='grey' name='twitter' />
        </IconWrapper>
      </FooterWrapper>
    </React.Fragment>
  )
}

export default Footer;