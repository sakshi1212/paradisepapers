import React from 'react'
import styled from 'styled-components'
import { Icon } from 'semantic-ui-react'
import Heading from '../atoms/Heading'

const HeaderWrapper = styled.div`
  height: 10vh;
  background: #242D34;
  padding: 10px 100px;
  display: flex;
`

const Logo = styled.div`
  color: white;
  width: 80%;
  display: flex;
  align-items: center
`

const IconWrapper = styled.div`
  display: flex;
  width: 30%;
  justify-content: flex-end;
  align-items: center;
  color: white;
`

const StyledHeading = styled(Heading)`
  padding-bottom: 20px;
`

const RightMenuLinks = styled.div`
  margin-left: 25px;
  &:hover{
    color: #F6543A;
    cursor: pointer;
  }
`

const Header = () => {
  return (
    <React.Fragment>
      <HeaderWrapper>
        <Logo>
          <Icon name='home' size='big' />
          <StyledHeading level={3}>My Logo</StyledHeading>
        </Logo>
        <IconWrapper>
          <RightMenuLinks>Home</RightMenuLinks>
          <RightMenuLinks>About Us</RightMenuLinks>
          <RightMenuLinks>Support</RightMenuLinks>
        </IconWrapper>
      </HeaderWrapper>
    </React.Fragment>
  )
}

export default Header;