import React, { PureComponent } from 'react';
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer
} from 'recharts';

const strokes = ['#0088FE', '#00C49F', '#FFBB28'];

class PlottedLineChart extends PureComponent {
  render() {
    const dataKeys = this.props.dataKeys;
    return (
      <ResponsiveContainer
        height={400}
        width={'100%'}
      >
        <LineChart
          width={500}
          height={300}
          data={this.props.chartData}
          margin={{
            top: 5, right: 30, left: 20, bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          {
            dataKeys.map((dataKey, index) => {
              return (
                <Line key={dataKey} type="monotone" dataKey={dataKey} stroke={strokes[index]} activeDot={{ r: 2 }} /> 
              )
            })
          }

        </LineChart>
      </ResponsiveContainer>
    );
  }
}

export default PlottedLineChart
