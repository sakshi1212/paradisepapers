import React, { Component } from 'react'
import { Form } from 'semantic-ui-react'
import Radio from '../atoms/Radio'
import styled from 'styled-components'

const StyledForm = styled(Form)`
  &&& {
    width: 20%;
    display: flex;
    justify-content: flex-end;
  }
`

class DateToPlotOptions extends Component {
  constructor(props){
    super(props);
    this.state = {
      value: this.props.dataToPlotSelection,
    }
  }

  handleChange = (e, { value }) => this.setState({ value }, this.props.handleDataToPlotChange(value))
  
  render() {
    return (
      <StyledForm>
        <Form.Field>
          <Radio
            label='Incorporation Date'
            name='radioGroup'
            value='IncorporationDate'
            checked={this.state.value === 'IncorporationDate'}
            onChange={this.handleChange}
          />
        </Form.Field>
        <Form.Field>
          <Radio
            label='Closed Date'
            name='radioGroup'
            value='ClosedDate'
            checked={this.state.value === 'ClosedDate'}
            onChange={this.handleChange}
          />
        </Form.Field>
      </StyledForm>
    )
  }
}

export default DateToPlotOptions;