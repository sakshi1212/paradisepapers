import React, { Component } from 'react'
import { Dropdown } from 'semantic-ui-react'
import styled from 'styled-components'

const DropdownWrapper = styled.div`
  width: 100%;
` 

const ErrorMessage = styled.div`
  text-align: left;
  color: red;
`

const SelectMessage = styled.div`
  text-align: left;
  color: #00C49F;
  font-weight: bold;
`

class DropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      errorMessage: null,
    }
  }

  handleChange = (e, { value }) => {
    const maxOptions = this.props.maxOptions;
    if (value.length > maxOptions) {
      this.setState({
        error: true,
        errorMessage: `Max ${maxOptions} comparisions allowed.`,
      })
    } else {
      this.setState({
        error: false,
        errorMessage: null,
      }, this.props.handleDropdownChange(value))
    }
  }

  render() {
    const { options, placeholder, onChange, multiple, handleDropdownChange, maxOptions, ...rest } = this.props;
    return (
      <DropdownWrapper>
        {this.state.error ? 
          <ErrorMessage>{this.state.errorMessage}</ErrorMessage> :
          <SelectMessage>Select up to 3 options</SelectMessage>
        }
        <Dropdown
          placeholder={placeholder}
          fluid
          selection
          multiple={multiple}
          options={options}
          onChange={this.handleChange}
          error={this.state.error}
          {...rest}
        />
      </DropdownWrapper>
    )
  }
}

export default DropDown