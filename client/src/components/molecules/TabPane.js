import React from 'react'
import { Tab } from 'semantic-ui-react'

const TabPane = ({ content, attached, loading }) => (
  <Tab.Pane 
    attached={attached}
    loading={loading}
  >
    {content}
  </Tab.Pane>
)

export default TabPane;