import React, { Component } from 'react'
import styled from 'styled-components'
import Heading from '../atoms/Heading'
import Text from '../atoms/Text'
import TimeDurationOptions from '../organisms/TimeDurationOptions'
import DateToPlotOptions from '../organisms/DateToPlotOptions'
import LineChart from '../organisms/LineChart'
import Dropdown from '../molecules/DropDown'
import axios from 'axios'
import groupBy from 'lodash/groupBy'
import camelCase from 'lodash/camelCase'
import flatten from 'lodash/flatten'
import merge from 'lodash/merge'
import get from 'lodash/get'
import moment from 'moment'

const OverviewWrapper = styled.div`
  padding: 40px 0px;
`  

const ComparisionOption = styled.div`
  height: 100px;
  display: flex;
  margin: 20px 0px;
  position: relative;
`

const BenchmarkingOptions = styled.div`
  padding: 20px 0px;
  display: flex;
`

const IndexOption = styled.div`
  display: flex;
  align-items: center;
  padding: 0px 40px;
`

const OwnIndexWrapper = styled(IndexOption)`
  background: #F7F7F7;
  justify-content: right;
  text-align: left;
  border-radius: 5px 0px 0px 5px;
  width: 30%;
`

const OtherIndexWrapper = styled(IndexOption)`
  background: #EBECEF;
  justify-content: center;
  border-radius: 0px 5px 5px 0px;
  width: 70%;
`

const LineChartWrapper = styled.div`
  background: #242D34;
  border-radius: 5px; 
  padding: 20px;
`

class EntitiesSection extends Component {

  constructor(props) {
    super(props);
    this.state = {
      chartData: [],
      jurisditionOptionsLoading: false,
      jurisditionOptions: [],
      jurisdictionDefaultValue: ['AW', 'MLT'],
      timeSelection: '3Y',
      jurisdictionSelection: ['AW', 'MLT'],
      dataToPlot: 'IncorporationDate'
    }
  }

  componentDidMount() {
    this.getJurisdictionOptions();
    this.getChartData();
  }

  getJurisdictionOptions = () => {
    this.setState({
      jurisditionOptionsLoading: true,
    })
    axios.get('/entities/getJurisdictions')
      .then((result) => {
        const data = get(result, 'data', []);
        const jurisditionOptions = data.map(jur => {
          return {
            key: jur.jurisdiction,
            text: jur.jurisdiction,
            value: jur.jurisdiction,
          }
        });
        this.setState({
          jurisditionOptions,
          jurisditionOptionsLoading: false,
        })
      })
      .catch((error) => {
        console.log(error);
      });
  }

  handleDataToPlotChange = (value) => {
    this.setState({
      dataToPlot: value,
    }, this.getChartData);
  }


  handleTimeChange = (value) => {
    this.setState({
      timeSelection: value,
    }, this.getChartData);
  }

  handleJurisdictionOptionsChange = (value) => {
    this.setState({
      jurisdictionSelection: value,
    }, this.getChartData);
  }

  getTimeFilters = () => {
    const currentTimeSelection = this.state.timeSelection;
    const today = moment().format('YYYY-MM-DD');
    switch(currentTimeSelection) {
      case '1Y': return {
        startDate: moment().subtract(1, 'year').format('YYYY-MM-DD'),
        endDate: today,
      };
      case '2Y': return {
        startDate: moment().subtract(2, 'year').format('YYYY-MM-DD'),
        endDate: today,
      };
      case '3Y': return {
        startDate: moment().subtract(3, 'year').format('YYYY-MM-DD'),
        endDate: today,
      };
      case '4Y': return {
        startDate: moment().subtract(4, 'year').format('YYYY-MM-DD'),
        endDate: today,
      };
      case '5Y': return {
        startDate: moment().subtract(5, 'year').format('YYYY-MM-DD'),
        endDate: today,
      };
      default: return {
        startDate: moment().subtract(1, 'year').format('YYYY-MM-DD'),
        endDate: today,
      };
    }
  }

  getDataToPlot = () => {
    return this.state.dataToPlot;
  }

  getSelectedJurisdictions = () => {
    return this.state.jurisdictionSelection;
  }

  getChartData = () => {
    const timeFilters = this.getTimeFilters();
    const dataToPlot = this.getDataToPlot();
    const jurisdictions = this.getSelectedJurisdictions();
    Promise.all(
      jurisdictions.map(jur => axios.get(`/entities/get${dataToPlot}s`, { params: { ...timeFilters, jurisdiction: jur }}))
    )
      .then((resp) => {
        const chartData = flatten(resp.map(r => r.data));
        const groupedByDate = groupBy(chartData, `${camelCase(dataToPlot)}`);
        const structuredData = Object.keys(groupedByDate).map(date => {
          const dateData = merge(...groupedByDate[date]);
          const jurObj = {};
          jurisdictions.forEach(jurCode => jurObj[jurCode] = dateData[jurCode] || 0);
          return {
            name: moment(date).format('DD-MM-YYYY'),
            ...jurObj,
          }
        });
        this.setState({
          chartData: structuredData,
        });

      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    return (
      <OverviewWrapper>
        <Heading level={3}>Entities Dashboard</Heading>
        <ComparisionOption>
          <OwnIndexWrapper>
            <Text level={2} palette={'#F6543A'} weight={'bold'}>Select Jurisdictions To Compare</Text>
          </OwnIndexWrapper>
          <OtherIndexWrapper>
            <Dropdown 
              options={this.state.jurisditionOptions}
              placeholder={'Select Jurisdictions to compare'}
              loading={this.state.jurisditionOptionsLoading}
              defaultValue={this.state.jurisdictionDefaultValue}
              handleDropdownChange={this.handleJurisdictionOptionsChange}
              maxOptions={3}
              multiple
            />
          </OtherIndexWrapper>
        </ComparisionOption>

        <BenchmarkingOptions>
          <TimeDurationOptions 
            timeSelection={this.state.timeSelection}
            handleTimeChange={this.handleTimeChange}
          />
          <DateToPlotOptions 
            dataToPlotSelection={this.state.dataToPlot}
            handleDataToPlotChange={this.handleDataToPlotChange}
          />
        </BenchmarkingOptions>

        <LineChartWrapper>
          <LineChart 
            chartData={this.state.chartData}
            dataKeys={this.state.jurisdictionSelection}
          />
        </LineChartWrapper>


      </OverviewWrapper>
    )
  }
}

export default EntitiesSection;