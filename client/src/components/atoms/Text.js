import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { switchProp, prop } from 'styled-tools'

const styles = css`
  color: ${prop('palette')}
  font-weight: ${switchProp(prop('weight'), {
    normal: '400',
    bold: '700'
  })};
  font-size: ${switchProp(prop('level'), {
    1: '16px',
    2: '14px',
    3: '12px',
  })};
  line-height: ${switchProp(prop('level'), {
    1: '16px',
    2: '14px',
    3: '12px',
  })};
  margin-bottom: 0.25rem;
`

const StyledText = styled.div`
  ${styles};
`
const Text = props => {
  return <StyledText {...props} />
}

Text.propTypes = {
  level: PropTypes.number,
  weight: PropTypes.string,
  palette: PropTypes.string,
}

Text.defaultProps = {
  level: 1,
  weight: 'normal',
  palette: 'gray',
}

export default Text
