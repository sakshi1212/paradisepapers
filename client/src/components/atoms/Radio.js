import React from 'react';
import styled from 'styled-components';
import { Radio } from 'semantic-ui-react'


const StyledRadio = styled(Radio) `
  &&& {
    min-width: 80px;
    padding: 10px 15px;
    background: white;
    font-weight: bold;
    margin-right: 5px;
    border-radius: 4px;
    > label {
      padding-left: 0px;
      color: #F6543A;
    }
    > label:before {
      display: none
    }
    > label:after {
      display: none;
    }
    &.checked {
      background: #F6543A;
      > label {
        color: white;
      }
    }
  }
`

const RadioOption = ({ label, ...props }) => {
  return (
    <StyledRadio 
      label={label}
      {...props}
    />
  );
};

export default RadioOption;