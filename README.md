# paradisePapers

### Summary

Simple webpage that allows to visualize data in a node, allows user to make selections of data he/she would like to view

### Pre-requistes

1. Node v8.12.0  
2. React ^16.9.0  
3. MySql

### Config Variables setup

1. Go to the folder : `./config/environment`  
2. To run locally, set up a `default-env.js` file. You may use the following as is 

`process.env.NODE_ENV = 'development';`

`process.env.PORT = '8080';`

`process.env.ADMIN_URL = 'http://localhost:3000';`

`process.env.DB_NAME = 'paradisePapers';`

`process.env.DB_HOST = 'localhost';`

`process.env.DB_USER = 'root';  `  

`process.env.DB_PASSWORD = '123123123';  ` 

  
5. To replicate an environment locally, Environment specific config can be put into the respective config file ie. staging.js, production.js.  


### Local Installation and Start Server

To install locally, for Server : At root directory  
1. `npm install` to install the libraries  
2. `npm i -g sequelize-cli` to allow us to use sequelize cli  
3. `sequelize db:create` will create the DB using the config provided  
4. `sequelize db:migrate` will migrate the required tables  
5. `sequelize db:seed:all` will seed required data

**The above 5 steps can also be run by a single line, to save time use : `npm run setup` **  

After installation, to start server, at root directory run:   
1. `npm run start`  

To run with nodemon for local development, use  
1. `npm run start:dev`  

The server will now be running at port 8080.  


### Local Installation and Start Client  

To start client, Inside `/client` folder  
1. run `npm install` to install the libraries for the client  
2. `npm run start` to start.   

The app is now accessible at http://localhost:3000  

