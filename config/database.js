const { mysql } = require('./environment');

const configDefaults = {
  dialect: 'mysql',
};

const config = {
  development: {
    username: mysql.username,
    password: mysql.password,
    database: mysql.name,
    host: mysql.host,
    ...configDefaults,
  },
  staging: {
    username: mysql.username,
    password: mysql.password,
    database: mysql.name,
    host: mysql.host,
    ...configDefaults,
  },
  production: {
    username: mysql.username,
    password: mysql.password,
    database: mysql.name,
    host: mysql.host,
    ...configDefaults,
  },
};

console.log(`Running sequelize-cli on host: ${config.host}`);

module.exports = {
  ...config,
};
