const express = require('express');
const controller = require('./controller');

const router = express.Router();

router.get('/getJurisdictions', controller.getJurisdictions);
router.get('/getIncorporationDates', controller.getIncorporationDates);
router.get('/getClosedDates', controller.getClosedDates);
router.get('/', controller.index);
router.get('/:id', controller.show);


module.exports = router;
