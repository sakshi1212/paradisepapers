const asyncMW = require('../../../middleware/async');
const omitBy = require('lodash/omitBy');
const isNil = require('lodash/isNil');
const { getTimeRangeQuery } = require('../../../utils/sequelize');

exports.index = asyncMW(async (req, res) => {
  try {
    const entities = await DB.Entity.findAll({
      limit: 2000,
    });
    return res.status(200).send(entities);
  } catch (err) {
    console.log(err);
    throw new HttpError(500, err.message);
  }
});

exports.getJurisdictions = asyncMW(async (req, res) => {
  try {
    const entities = await DB.Entity.findAll({
      attributes:[[sequelize.literal('distinct `jurisdiction`'),'jurisdiction']]
    });
    return res.status(200).send(entities);
  } catch (err) {
    console.log(err);
    throw new HttpError(500, err.message);
  }
});

exports.getIncorporationDates = asyncMW(async (req, res) => {
  const {
    startDate,
    endDate,
    jurisdiction,
  } = req.query;
  try {
    const entities = await DB.Entity.findAll({
      where: omitBy({
        jurisdiction,
        ...getTimeRangeQuery('incorporationDate', { startDate, endDate }),
      }, isNil),
      attributes: ['incorporationDate', [sequelize.fn('count', sequelize.col('incorporationDate')), `${jurisdiction}`]],
      group: ['incorporationDate'],
      order: [['incorporationDate', 'asc']],
    });
    return res.status(200).send(entities);
  } catch (err) {
    console.log(err);
    throw new HttpError(500, err.message);
  }
});

exports.getClosedDates = asyncMW(async (req, res) => {
  const {
    startDate,
    endDate,
    jurisdiction,
  } = req.query;
  try {
    const entities = await DB.Entity.findAll({
      where: omitBy({
        jurisdiction,
        ...getTimeRangeQuery('closedDate', { startDate, endDate }),
      }, isNil),
      attributes: ['closedDate', [sequelize.fn('count', sequelize.col('closedDate')), `${jurisdiction}`]],
      group: ['closedDate'],
      order: [['closedDate', 'asc']],
    });
    return res.status(200).send(entities);
  } catch (err) {
    console.log(err);
    throw new HttpError(500, err.message);
  }
});

exports.show = asyncMW(async (req, res) => {
  const { id } = req.params;
  try {
    const entity = await DB.Entity.findOne({
      where: {
        id,
      },
      rejectOnEmpty: new HttpError(404, `Entity with id ${id} not found`),
    });
    return res.status(200).send({
      data: entity,
    });
  } catch (err) {
    console.log(err);
    throw new HttpError(500, err.message);
  }
});

