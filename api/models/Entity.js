const Sequelize = require('sequelize');

const { DataTypes } = Sequelize;

class Entity extends Sequelize.Model {

  static init(sequelize) {
    return super.init(
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER,
        },
        nodeId: {
          type: DataTypes.INTEGER,
          allowNull: false,
          references: {
            model: 'nodes',
            key: 'id',
          },
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        jurisdiction: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        jurisdictionDescription: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        countryCodes: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        countries: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        incorporationDate: {
          type: DataTypes.DATE,
          allowNull: true,
        },
        inactivationDate: {
          type: DataTypes.DATE,
          allowNull: true,
        },
        struckOffDate: {
          type: DataTypes.DATE,
          allowNull: true,
        },
        closedDate: {
          type: DataTypes.DATE,
          allowNull: true,
        },
        ibcRUC: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        status: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        companyType: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        serviceProvider: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        sourceId: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        validUntil: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        note: {
          type: DataTypes.STRING,
          allowNull: true,
        },
      },
      {
        sequelize,
        modelName: 'entities',
      }
    );
  }

  static associate(models) {
  }
}

module.exports = Entity;
