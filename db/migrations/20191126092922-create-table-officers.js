module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('officers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      nodeId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'nodes',
          key: 'id',
        },
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      address: {
        type: Sequelize.STRING,
      },
      countryCodes: {
        type: Sequelize.STRING,
      },
      countries: {
        type: Sequelize.STRING,
      },
      sourceId: {
        type: Sequelize.STRING,
      },
      validUntil: {
        type: Sequelize.STRING,
      },
      note: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('officers');
  },
};

// "node_id","name","country_codes","countries","status","sourceID","valid_until","note"
