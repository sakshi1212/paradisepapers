module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('intermediatories', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      nodeId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'nodes',
          key: 'id',
        },
      },
      countryCodes: {
        type: Sequelize.STRING,
      },
      countries: {
        type: Sequelize.STRING,
      },
      sourceId: {
        type: Sequelize.STRING,
      },
      validUntil: {
        type: Sequelize.STRING,
      },
      note: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('intermediatories');
  },
};



// "node_id","name","country_codes","countries","sourceID","valid_until","note"