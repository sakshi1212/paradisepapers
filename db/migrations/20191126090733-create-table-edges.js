module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('edges', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      startId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'nodes',
          key: 'id',
        },
      },
      endId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'nodes',
          key: 'id',
        },
      },
      type: {
        type: Sequelize.ENUM,
        values: ['registered_address', 'connected_to', 'intermediary', 'officer', 'other'],
        allowNull: false,
      },
      link: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      startDate: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      endDate: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      sourceId: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      validUntill: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('edges');
  },
};
