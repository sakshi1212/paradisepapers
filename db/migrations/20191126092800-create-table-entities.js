module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('entities', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      nodeId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'nodes',
          key: 'id',
        },
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      jurisdiction: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      jurisdictionDescription: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      countryCodes: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      countries: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      incorporationDate: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      inactivationDate: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      struckOffDate: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      closedDate: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      ibcRUC: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      status: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      companyType: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      serviceProvider: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      sourceId: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      validUntil: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      note: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('entities');
  },
};
