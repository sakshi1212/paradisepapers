'use strict';
const path = require('path');
const csv = require('csvtojson');
const chunk = require('lodash/chunk');

const csvFilePath = path.join(__dirname, './data/paradise_papers.nodes.entity.csv');

module.exports = {
  up: async(queryInterface, Sequelize) => {
    const now = new Date();
    const timestamps = {
      createdAt: now,
      updatedAt: now,
    };
    console.log('==== Starting Entity Nodes Import ====');
    console.log('Getting entity nodes from csv ...'); 
    return csv()
      .fromFile(csvFilePath)
      .then(async(entityNodes) => {
        console.log('Total nodes found in CSV: ', entityNodes.length);
        try {
          const chunckedArr = chunk(entityNodes, 100);
          for (const nodesChunk of chunckedArr) {
            console.log('====== Processing 100 products.... ');
            const createdNodes = await Promise.all(nodesChunk.map(async(node) => {
              const createdNodeId = await queryInterface.bulkInsert('nodes', [{
                nodeId: node.node_id,
                type: 'entity',
                ...timestamps,
              }], { returning: true });
              return {
                createdNodeId,
                ...node,
              }
            }));
            const structuredNodes = createdNodes.map(node => {
              return {
                nodeId: node.createdNodeId,
                name: node.name,
                jurisdiction: node.jurisdiction,
                jurisdictionDescription: node.jurisdiction_description,
                countryCodes: node.country_codes,
                countries: node.countries,
                incorporationDate: node.incorporation_date ? new Date(node.incorporation_date) : null,
                inactivationDate: node.inactivation_date ? new Date(node.inactivation_date) : null,
                struckOffDate: node.struck_off_date ? new Date(node.struck_off_date) : null,
                closedDate: node.closed_date ? new Date(node.closed_date) : null,
                ibcRUC: node.ibcRUC,
                status: node.status,
                companyType: node.company_type,
                serviceProvider: node.service_provider,
                sourceId: node.sourceID,
                validUntil: node.valid_until,
                note: node.note,
                ...timestamps,
              }
            });
            await queryInterface.bulkInsert('entities', structuredNodes);
          }
        } catch (err) {
          console.log(err);
        }
      })
  },

  down: (queryInterface) => {
    return queryInterface.sequelize.transaction(transaction => {
    });
  },

}
