'use strict';
const path = require('path');
const csv = require('csvtojson');
const chunk = require('lodash/chunk');

const csvFilePath = path.join(__dirname, './data/paradise_papers.nodes.officer.csv');

module.exports = {
  up: async(queryInterface, Sequelize) => {
    const now = new Date();
    const timestamps = {
      createdAt: now,
      updatedAt: now,
    };
    console.log('==== Starting Entity Nodes Import ====');
    console.log('Getting address nodes from csv ...'); 
    return csv()
      .fromFile(csvFilePath)
      .then(async(entityNodes) => {
        console.log('Total nodes found in CSV: ', entityNodes.length);

        // const testNodes = entityNodes.slice(0, 2);
  
        const chunckedArr = chunk(entityNodes, 100);
        for (const nodesChunk of chunckedArr) {
          console.log('====== Processing 100 products.... ');
          const createdNodes = await Promise.all(nodesChunk.map(async(node) => {
            const createdNodeId = await queryInterface.bulkInsert('nodes', [{
              nodeId: node.node_id,
              type: 'officer',
              ...timestamps,
            }], { returning: true });
            return {
              createdNodeId,
              ...node,
            }
          }));
          const structuredNodes = createdNodes.map(node => {
            return {
              nodeId: node.createdNodeId,
              name: node.name,
              address: node.address,
              countryCodes: node.country_codes,
              countries: node.countries,
              sourceId: node.sourceID,
              validUntil: node.valid_until,
              note: node.note,
              ...timestamps,
            }
          });
          await queryInterface.bulkInsert('officers', structuredNodes);
          console.log('inserted')
        }
      });
    },
          


  down: (queryInterface) => {
    return queryInterface.sequelize.transaction(transaction => {
    });
  },

}